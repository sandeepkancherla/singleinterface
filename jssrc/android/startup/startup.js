//startup.js file
var globalhttpheaders = {};
var appConfig = {
    appId: "OauthSpreadsht",
    appName: "OauthSpreadsht",
    appVersion: "1.0.0",
    platformVersion: null,
    serverIp: "citius2.konylabs.net",
    serverPort: "80",
    secureServerPort: "443",
    isDebug: true,
    middlewareContext: "OauthSpreadsht",
    url: "http://citius2.konylabs.net:80/OauthSpreadsht/MWServlet",
    secureurl: "https://citius2.konylabs.net:443/OauthSpreadsht/MWServlet"
};
sessionID = "";
gblAccessCode = "";

function appInit(params) {
    skinsInit();
    frmOauthTestGlobals();
    setAppBehaviors();
};

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true
    })
};

function themeCallBack() {
    kony.application.setApplicationInitializationEvents({
        init: appInit,
        showstartupform: function() {
            frmOauthTest.show();
        }
    });
};

function loadResources() {
    globalhttpheaders = {};
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
};
kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
//If default locale is specified. This is set even before any other app life cycle event is called.
loadResources();
// If you wish to debug Application Initialization events, now is the time to
// place breakpoints.
debugger;