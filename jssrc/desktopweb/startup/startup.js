//startup.js file
var appConfig = {
    appId: "OauthSpreadsht",
    appName: "OauthSpreadsht",
    appVersion: "1.0.0",
    platformVersion: null,
    serverIp: "10.20.1.81",
    serverPort: "8080",
    secureServerPort: "443",
    url: "http://10.20.1.81:8080/middleware/MWServlet",
    secureurl: "https://10.20.1.81:443/middleware/MWServlet",
    middlewareContext: "middleware"
};
sessionID = "";

function appInit(params) {
    skinsInit();
    frmOauthTestGlobals();
    setAppBehaviors();
};

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true
    })
};

function themeCallBack() {
    kony.application.setApplicationInitializationEvents({
        init: appInit,
        showstartupform: function() {
            frmOauthTest.show();
        }
    });
};

function loadResources() {
    kony.theme.packagedthemes(
    ["default"]);
    globalhttpheaders = {};
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
};

function initializeApp() {
    kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
    //If default locale is specified. This is set even before any other app life cycle event is called.
    loadResources();
};