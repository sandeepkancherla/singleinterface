gblDeviceInfo = kony.os.deviceInfo();
//client_id="856969395944-14prt3obbh1v97m49m18bltfl12p3rsr.apps.googleusercontent.com"
client_id = "1011438646456-vdjksajaoe6n5e5v699ml72j3qb5ebnk.apps.googleusercontent.com"
scope = "https://spreadsheets.google.com/feeds%20https://www.googleapis.com/auth/drive"
redirect_uri = "urn:ietf:wg:oauth:2.0:oob"
client_sec = "xkpK4MzLPTVKRhbxqQBX113J"

function getOauthCode() {
    var url = "https://accounts.google.com/o/oauth2/auth?scope=" + scope + "&redirect_uri=" + redirect_uri + "&response_type=code&client_id=" + client_id
    kony.application.openURL(url);
}

function getOauthAccessToken() {
    var AuthCode = frmOauthTest.textbox2450035236169612.text;
    //alert("AuthCode: "+AuthCode)
    kony.print("AuthCode: " + AuthCode);
    var inputParams = {};
    inputParams.serviceID = "getOauthToken"
    inputParams["httpheaders"] = {}
    inputParams["channel"] = "wap";
    inputParams.appID = appConfig.appId;
    inputParams.appver = appConfig.appVersion;
    inputParams["locale"] = "en_US" //kony.i18n.getCurrentLocale();
    inputParams["platform"] = gblDeviceInfo.name;
    inputParams.accessCode = AuthCode;
    inputParams.client_id = client_id;
    inputParams.client_secret = client_sec;
    inputParams.redirect_uri = redirect_uri;
    inputParams.grant_type = "authorization_code";
    var url = "http://citius2.konylabs.net/OauthSpreadsht/MWServlet";
    kony.print("inputParams :::" + inputParams);
    var connHandle = kony.net.invokeServiceAsync(url, inputParams, callbackFunc)
}

function callbackFunc(status, resultSet) {
    if (status == 200) {
        if (resultSet["opstatus"] == 0) {
            if (resultSet != null && resultSet.length > 0)
            //alert(JSON.stringify(resultSet));
            {
                var access_tok = resultSet["access_token"];
                var refresh_token = resultSet["refresh_token"];
                var expireTime = resultSet["expires_in"];
                if (access_tok != null || access_tok != "") {
                    var inputParams = {};
                    inputParams.httpheaders = {};
                    inputParams.serviceID = "ReadSpreadsheets";
                    inputParams["channel"] = "wap";
                    inputParams.appID = appConfig.appId;
                    inputParams.appver = appConfig.appVersion;
                    inputParams["locale"] = "en_US" //kony.i18n.getCurrentLocale();
                    inputParams["platform"] = gblDeviceInfo.name;
                    inputParams.accessToken = access_tok;
                    inputParams.refreshtoken = refresh_token;
                    inputParams.expireTime = expireTime;
                    inputParams.client_id = client_id;
                    inputParams.client_secret = client_sec;
                    inputParams.redirect_uri = redirect_uri;
                    var url = "http://citius2.konylabs.net/OauthSpreadsht/MWServlet";
                    var connHandle = kony.net.invokeServiceAsync(url, inputParams, callbackFuncReadSheets)
                } else alert("error access token not found ");
            } else alert("service error");
        }
    }
}

function callbackFuncReadSheets(status, resultSet) {
    if (status == 400) {
        if (resultSet["opstatus"] == 0) {
            if (resultSet != null && resultSet.length > 0)
            //alert(JSON.stringify(resultSet));
            {
                for (var i = 0; i < resultSet.length; i++) {
                    var len = resultSet[i].length;
                    for (j = 0; j < len; j++) {
                        alert(resultSet[i][j]);
                    }
                }
            }
        }
    }
}