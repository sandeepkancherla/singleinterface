//Form JS File
function frmOauthTest_btnOauth_onClick_seq0(eventobject) {
    getOauthCode.call(this);
};

function frmOauthTest_btnSend_onClick_seq0(eventobject) {
    getOauthAccessToken.call(this);
};

function addWidgetsfrmOauthTest() {
    var btnOauth = new kony.ui.Button({
        "id": "btnOauth",
        "isVisible": true,
        "text": "Get Auth Code",
        "skin": "btnNormal",
        "focusSkin": "btnFocus",
        "onClick": frmOauthTest_btnOauth_onClick_seq0
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 3, 0, 3],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 6
    }, {});
    var textbox2450035236169612 = new kony.ui.TextBox2({
        "id": "textbox2450035236169612",
        "isVisible": true,
        "text": "TextBox2",
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "placeholder": null,
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "skin": "tbx2Normal",
        "focusSkin": "tbx2Focus"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 6
    }, {
        "autoCorrect": false,
        "autoComplete": false
    });
    var btnSend = new kony.ui.Button({
        "id": "btnSend",
        "isVisible": true,
        "text": "Get Auth Access",
        "skin": "btnNormal",
        "focusSkin": "btnFocus",
        "onClick": frmOauthTest_btnSend_onClick_seq0
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 3, 0, 3],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 6
    }, {});
    frmOauthTest.add(
    btnOauth, textbox2450035236169612, btnSend);
};

function frmOauthTestGlobals() {
    var MenuId = [];
    frmOauthTest = new kony.ui.Form({
        "id": "frmOauthTest",
        "title": null,
        "skin": "frm",
        "addWidgets": addWidgetsfrmOauthTest
    }, {
        "padding": [0, 0, 0, 0],
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {
        "retainScrollPosition": false,
        "needAppLevelMenu": true,
        "enabledForIdleTimeout": false,
        "formType": constants.FORM_TYPE_NATIVE,
        "formTransition": "None"
    });
};