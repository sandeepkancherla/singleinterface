//startup.js file
var appConfig = {
    appId: "OauthSpreadsht",
    appName: "OauthSpreadsht",
    appVersion: "1.0.0",
    platformVersion: null,
    serverIp: "10.20.17.131",
    serverPort: "8081",
    secureServerPort: "443",
    url: "http://10.20.17.131:8081/OauthSpreadsht/MWServlet",
    secureurl: "https://10.20.17.131:443/OauthSpreadsht/MWServlet",
    middlewareContext: "OauthSpreadsht"
};
sessionID = "";

function appInit(params) {
    skinsInit();
    frmOauthTestGlobals();
};

function themeCallBack() {
    kony.application.setApplicationInitializationEvents({
        init: appInit,
        showstartupform: function() {
            frmOauthTest.show();
        }
    });
};

function loadResources() {
    kony.theme.packagedthemes(
    ["default"]);
    globalhttpheaders = {};
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
};

function initializeApp() {
    kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false
    })
    //If default locale is specified. This is set even before any other app life cycle event is called.
    loadResources();
};